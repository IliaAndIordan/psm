export const environment = {
  production: false,
  services: {
    url: {

      auth: 'https://sx.ws.iordanov.info/',
      company: 'https://sx.ws.iordanov.info/company/',
      pricefile: 'https://sx.ws.iordanov.info/pricefile/',
      project: 'https://sx.ws.iordanov.info/project/',
      commodity: 'https://sx.ws.iordanov.info/mfr/',
    },
    domains: {
      iordanovInfo: 'sx.ws.iordanov.info',
    },
  }
};
